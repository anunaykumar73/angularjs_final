configuration Website 
{ 
Import-DscResource -Module xWebAdministration 
Node localhost 
    { 
    xWebsite DefaultSite  
        { 
            Ensure          = "Present" 
            Name            = "Default Web Site" 
            State           = "Stopped" 
            PhysicalPath    = "C:\inetpub\wwwroot" 
            
        } 
        
        xWebsite NewWebsite 
        { 
             
            Name            = "AngularJS"
            Ensure          = "Present" 
            State           = "Started" 
            PhysicalPath    = "C:\inetpub\wwwroot\AngularPractice"
            
           
        }   
        }
        }
      Website -output C:\users\anunay.kumar\desktop
        Start-DscConfiguration -Path C:\users\anunay.kumar\desktop -Wait -Force -Verbose