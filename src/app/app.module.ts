// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { EmployeeComponent } from './components/employee/employee.component';

// Services
import { AppService } from './services/app.service';
import { EmployeeService } from './services/employee.service';
import { FakeBackendInterceptor } from './services/fakebackend.service';


@NgModule({
  imports: [
    BrowserModule, ReactiveFormsModule, FormsModule, HttpClientModule
  ],
  declarations: [
    AppComponent, EmployeeComponent
  ],
  providers: [AppService, EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
