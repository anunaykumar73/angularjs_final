import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee.model';
import { PasswordValidation } from '../../utilities/password.validation';
import { RegexValidators } from '../../utilities/regex-validator';

@Component({
  selector: 'employee-list',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers: [EmployeeService]
})
export class EmployeeComponent implements OnInit {
  employee: FormGroup;
  employees : Employee[] = [];
  isEditMode: boolean = false;

  constructor(private fb: FormBuilder, private empService: EmployeeService){
    this.getEmployees();
  }

  ngOnInit() {
    this.employee = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(RegexValidators.email)]],
      passwords: this.fb.group({
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      },{ validator: PasswordValidation.MatchPassword })
    });
  }

  getEmployees(){
    this.empService.getEmployees().subscribe(
      (employees) => {
        this.employees = employees;
      }
    );
  }

  saveEmployee({ employee, valid }: { employee: Employee, valid: boolean }){
    if(this.isEditMode){
      this.empService.updateEmployee(employee).subscribe(() => {
          console.log('Employee updated successfully');
          this.getEmployees();
        }
      );
    }
    else{
      this.empService.saveEmployee(employee).subscribe(() => {
          console.log('Employee added successfully');
          this.getEmployees();
        }
      );
    }
    this.reset();
  }

  setEditMode(employee){
    this.employee.setValue(employee);
    this.isEditMode = true;
  }

  reset(){
    this.employee.reset();
    this.isEditMode = false;
    return false;
  }

  deleteEmployee(employee){
    this.empService.deleteEmployee(employee.email).subscribe(() => {
        console.log('Employee deleted successfully');
        this.getEmployees();
      }
    );
  }
}
